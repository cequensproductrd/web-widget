import axios from 'axios';
var BotMan = (function () {
    function BotMan() {
        var _this = this;
        this.callAPI = function (text, interactive, attachment, perMessageCallback, callback) {
            if (interactive === void 0) { interactive = false; }
            if (attachment === void 0) { attachment = null; }
            var data = new FormData();
            var postData = {
                driver: 'web',
                userId: _this.userId,
                message: text,
                attachment: attachment,
                interactive: interactive ? '1' : '0'
            };
            Object.keys(postData).forEach(function (key) { return data.append(key, postData[key]); });
            axios.post(_this.chatServer, data).then(function (response) {
                var messages = response.data.messages || [];
                if (perMessageCallback) {
                    messages.forEach(function (msg) {
                        perMessageCallback(msg);
                    });
                }
                if (callback) {
                    callback(response.data);
                }
            });
        };
    }
    BotMan.prototype.setUserId = function (userId) {
        this.userId = userId;
    };
    BotMan.prototype.setChatServer = function (chatServer) {
        this.chatServer = chatServer;
    };
    return BotMan;
}());
export var botman = new BotMan();
//# sourceMappingURL=botman.js.map