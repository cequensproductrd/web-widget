import { h, render } from 'preact';
import Chat from './chat';
if (window.attachEvent) {
    window.attachEvent('onload', injectChat);
}
else {
    window.addEventListener('load', injectChat, false);
}
var conf = {};
var confString = getUrlParameter('conf');
if (confString) {
    try {
        conf = JSON.parse(confString);
    }
    catch (e) {
        console.error('Failed to parse conf', confString, e);
    }
}
function injectChat() {
    var root = document.createElement('div');
    root.id = 'botmanChatRoot';
    document.getElementsByTagName('body')[0].appendChild(root);
    render(h(Chat, { userId: conf.userId, conf: conf }), root);
}
function getUrlParameter(name) {
    name = name.replace(/[[]/, '\\[').replace(/[]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
//# sourceMappingURL=index.js.map