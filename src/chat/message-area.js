var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { h, Component } from 'preact';
import MessageHolder from "./message-holder";
var MessageArea = (function (_super) {
    __extends(MessageArea, _super);
    function MessageArea() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MessageArea.prototype.render = function (props, _a) {
        var styleChat = 'height:' + (props.conf.wrapperHeight - 60) + 'px;';
        var calculatedTimeout = 0;
        return (h("ol", { class: "chat", style: styleChat }, props.messages.map(function (message) {
            var listElement = h(MessageHolder, { message: message, calculatedTimeout: calculatedTimeout, messageHandler: props.messageHandler, conf: props.conf });
            calculatedTimeout += message.timeout * 1000;
            return listElement;
        })));
    };
    return MessageArea;
}(Component));
export default MessageArea;
;
//# sourceMappingURL=message-area.js.map