var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { h, Component } from 'preact';
import * as dateFormat from "dateformat";
import TextType from "./messages/text";
import ActionType from "./messages/action";
import TypingIndicator from "./messages/typing-indicator";
import ListType from "./messages/list";
import ButtonsType from "./messages/buttons";
var dayInMillis = 60 * 60 * 24 * 1000;
var messageTypes = {
    actions: ActionType,
    buttons: ButtonsType,
    list: ListType,
    text: TextType,
    typing_indicator: TypingIndicator
};
var MessageHolder = (function (_super) {
    __extends(MessageHolder, _super);
    function MessageHolder() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.scrollToBottom = function () {
            var messageArea = document.getElementById('messageArea');
            messageArea.scrollTop = messageArea.scrollHeight;
        };
        _this.messageVisibilityChange = function (message, messageState) {
            var msg = _this.props.message;
            if (msg.id === message.id && msg.visible !== messageState.visible) {
                msg.visible = messageState.visible;
                // Reset the timeout
                msg.timeout = 0;
                _this.forceUpdate();
            }
        };
        return _this;
    }
    MessageHolder.prototype.componentDidMount = function () {
        this.scrollToBottom();
    };
    MessageHolder.prototype.componentDidUpdate = function () {
        this.scrollToBottom();
    };
    MessageHolder.prototype.render = function (props) {
        var currentTime = new Date();
        var message = props.message;
        var msgTime = new Date(message.time);
        var MessageComponent = messageTypes[message.type] || TextType;
        var _a = this.props, messageHandler = _a.messageHandler, conf = _a.conf;
        var styles = '';
        if (message.visible === false || message.visibilityChanged === false) {
            styles += 'display:none';
        }
        var calculatedTimeout = props.calculatedTimeout;
        return (h("li", { "data-message-id": message.id, class: message.from, style: styles },
            h("div", { class: "msg" },
                h(MessageComponent, { onVisibilityChange: this.messageVisibilityChange, message: message, timeout: calculatedTimeout, messageHandler: messageHandler, conf: conf }),
                (props.conf.displayMessageTime) ?
                    h("div", { class: "time" }, currentTime.getMilliseconds() - msgTime.getMilliseconds() < dayInMillis ?
                        dateFormat(msgTime, props.conf.timeFormat) :
                        dateFormat(msgTime, props.conf.dateTimeFormat))
                    :
                        '')));
    };
    return MessageHolder;
}(Component));
export default MessageHolder;
//# sourceMappingURL=message-holder.js.map