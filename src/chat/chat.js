var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { h, Component } from "preact";
import MessageArea from "./message-area";
import { botman } from "./botman";
var Chat = (function (_super) {
    __extends(Chat, _super);
    function Chat(props) {
        var _this = _super.call(this, props) || this;
        _this.handleKeyPress = function (e) {
            if (e.keyCode === 13 && _this.input.value.replace(/\s/g, "")) {
                _this.say(_this.input.value);
                // Reset input value
                _this.input.value = "";
            }
        };
        _this.handleSendClick = function (e) {
            _this.say(_this.textarea.value);
            // Reset input value
            _this.textarea.value = "";
        };
        _this.writeToMessages = function (msg) {
            if (typeof msg.time === "undefined") {
                msg.time = new Date().toJSON();
            }
            if (typeof msg.visible === "undefined") {
                msg.visible = false;
            }
            if (typeof msg.timeout === "undefined") {
                msg.timeout = 0;
            }
            if (typeof msg.id === "undefined") {
                msg.id = Chat.generateUuid();
            }
            if (msg.attachment === null) {
                msg.attachment = {}; // TODO: This renders IAttachment useless
            }
            _this.state.messages.push(msg);
            _this.setState({
                messages: _this.state.messages
            });
            if (msg.additionalParameters && msg.additionalParameters.replyType) {
                _this.setState({
                    replyType: msg.additionalParameters.replyType
                });
            }
        };
        _this.botman = botman;
        _this.botman.setUserId(_this.props.userId);
        _this.botman.setChatServer(_this.props.conf.chatServer);
        _this.state.messages = [];
        _this.state.replyType = ReplyType.Text;
        return _this;
    }
    Chat.prototype.componentDidMount = function () {
        var _this = this;
        if (!this.state.messages.length && this.props.conf.introMessage) {
            this.writeToMessages({
                text: this.props.conf.introMessage,
                type: "text",
                from: "chatbot"
            });
        }
        // Add event listener for widget API
        window.addEventListener("message", function (event) {
            try {
                _this[event.data.method].apply(_this, event.data.params);
            }
            catch (e) {
                //
            }
        });
    };
    Chat.prototype.sayAsBot = function (text) {
        this.writeToMessages({
            text: text,
            type: "text",
            from: "chatbot"
        });
    };
    Chat.prototype.say = function (text, showMessage) {
        var _this = this;
        if (showMessage === void 0) { showMessage = true; }
        var message = {
            text: text,
            type: "text",
            from: "visitor"
        };
        // Send a message from the html user to the server
        this.botman.callAPI(message.text, false, null, function (msg) {
            msg.from = "chatbot";
            _this.writeToMessages(msg);
        });
        if (showMessage) {
            this.writeToMessages(message);
        }
    };
    Chat.prototype.whisper = function (text) {
        this.say(text, false);
    };
    Chat.prototype.render = function (_a, state) {
        var _this = this;
        return (h("div", null,
            h("div", { id: "messageArea" },
                h(MessageArea, { messages: state.messages, conf: this.props.conf, messageHandler: this.writeToMessages })),
            this.state.replyType === ReplyType.Text ? (h("input", { id: "userText", class: "textarea", type: "text", placeholder: this.props.conf.placeholderText, ref: function (input) {
                    _this.input = input;
                }, onKeyPress: this.handleKeyPress, autofocus: true })) : '',
            this.state.replyType === ReplyType.TextArea ? (h("div", null,
                h("svg", { xmlns: "http://www.w3.org/2000/svg", x: "0px", y: "0px", onClick: this.handleSendClick, style: "cursor: pointer; position: absolute; width: 25px; bottom: 19px; right: 16px; z-index: 1000", viewBox: "0 0 535.5 535.5" },
                    h("g", null,
                        h("g", { id: "send" },
                            h("polygon", { points: "0,497.25 535.5,267.75 0,38.25 0,216.75 382.5,267.75 0,318.75" })))),
                h("textarea", { id: "userText", class: "textarea", placeholder: this.props.conf.placeholderText, ref: function (input) {
                        _this.textarea = input;
                    }, autofocus: true }))) : '',
            h("a", { class: "banner", href: this.props.conf.aboutLink, target: "_blank" }, this.props.conf.aboutText === "AboutIcon" ? (h("svg", { style: "position: absolute; width: 14px; bottom: 6px; right: 6px;", fill: "#EEEEEE", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 1536 1792" },
                h("path", { d: "M1024 1376v-160q0-14-9-23t-23-9h-96v-512q0-14-9-23t-23-9h-320q-14 0-23 9t-9 23v160q0 14 9 23t23 9h96v320h-96q-14 0-23 9t-9 23v160q0 14 9 23t23 9h448q14 0 23-9t9-23zm-128-896v-160q0-14-9-23t-23-9h-192q-14 0-23 9t-9 23v160q0 14 9 23t23 9h192q14 0 23-9t9-23zm640 416q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z" }))) : (this.props.conf.aboutText))));
    };
    Chat.generateUuid = function () {
        var uuid = '', ii;
        for (ii = 0; ii < 32; ii += 1) {
            switch (ii) {
                case 8:
                case 20:
                    uuid += '-';
                    uuid += (Math.random() * 16 | 0).toString(16);
                    break;
                case 12:
                    uuid += '-';
                    uuid += '4';
                    break;
                case 16:
                    uuid += '-';
                    uuid += (Math.random() * 4 | 8).toString(16);
                    break;
                default:
                    uuid += (Math.random() * 16 | 0).toString(16);
            }
        }
        return uuid;
    };
    return Chat;
}(Component));
export default Chat;
var ReplyType;
(function (ReplyType) {
    ReplyType[ReplyType["Text"] = "text"] = "Text";
    ReplyType[ReplyType["TextArea"] = "textarea"] = "TextArea";
})(ReplyType || (ReplyType = {}));
//# sourceMappingURL=chat.js.map