var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { h, Component } from 'preact';
import { botman } from './botman';
var ChatAction = (function (_super) {
    __extends(ChatAction, _super);
    function ChatAction() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ChatAction.prototype.render = function (props) {
        var _this = this;
        return (h("div", { class: "btn", onClick: function () { return _this.performAction(props.action); } }, props.action.text));
    };
    ChatAction.prototype.performAction = function (action) {
        var _this = this;
        botman.callAPI(action.value, true, null, function (msg) {
            _this.props.messageHandler({
                text: msg.text,
                type: msg.type,
                actions: msg.actions,
                attachment: msg.attachment,
                additionalParameters: msg.additionalParameters,
                from: 'chatbot'
            });
        }, null);
    };
    return ChatAction;
}(Component));
export default ChatAction;
//# sourceMappingURL=chat-action.js.map