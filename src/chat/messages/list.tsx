import { h, Component } from 'preact';
import {botman} from '../botman';
import MessageType from "./messagetype";
import { IButton, IMessage, IMessageTypeProps } from '../../typings';

// import { NgxCarousel } from '@ngu/carousel'

export default class ListType extends MessageType {

    getButton(button: IButton) {
        if (button.type === 'postback') {
            return <div class="btn" onClick={() => this.performAction(button)}>
                {button.title}
            </div>;
        }
        if (button.type === 'web_url') {
            return <a class="btn" href={button.url} target="_blank">{button.title}</a>;
        }
    }

    render(props: IMessageTypeProps) {
        const message = props.message;

        const globalButtons = message.globalButtons.map((button: IButton) => {
            return this.getButton(button);
        });

        const lists = message.elements.map((element,index) => {
            const elementButtons = element.buttons.map((button: IButton) => {
                return this.getButton(button);
            });
                if (index ==0){
                   return  <div class="carousel-item active">
                        <img class="d-block w-100" src={element.image_url} alt={element.title}></img>
                       <div> {element.title}</div>
                       {elementButtons}
                    </div>
                }
            return  <div class="carousel-item">
                        <img class="d-block w-100" src={element.image_url} alt={element.title}></img>
                    <div> {element.title}</div>
                {elementButtons}
                    </div>
        });

        return (
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    {lists}
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        );
    }

    performAction(button: IButton) {
        botman.callAPI(button.payload, true, null, (msg: IMessage) => {
            this.props.messageHandler({
                text: msg.text,
                type: msg.type,
                actions: msg.actions,
                attachment: msg.attachment,
                additionalParameters: msg.additionalParameters,
                from: 'chatbot'
            });
        }, null)};
    }

