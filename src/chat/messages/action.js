var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { h } from 'preact';
import { botman } from './../botman';
import MessageType from "./messagetype";
var Action = (function (_super) {
    __extends(Action, _super);
    function Action() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Action.prototype.render = function (props) {
        var _this = this;
        var message = props.message;
        var buttons = message.actions.map(function (action) {
            return h("div", { class: "btn", onClick: function () { return _this.performAction(action); } }, action.text);
        });
        return (h("div", null,
            message.text && h("div", null, message.text),
            this.state.attachmentsVisible ?
                h("div", null, buttons)
                : ''));
    };
    Action.prototype.performAction = function (action) {
        var _this = this;
        botman.callAPI(action.value, true, null, function (msg) {
            _this.state.attachmentsVisible = false;
            _this.props.messageHandler({
                text: msg.text,
                type: msg.type,
                timeout: msg.timeout,
                actions: msg.actions,
                attachment: msg.attachment,
                additionalParameters: msg.additionalParameters,
                from: 'chatbot'
            });
        }, null);
    };
    return Action;
}(MessageType));
export default Action;
//# sourceMappingURL=action.js.map