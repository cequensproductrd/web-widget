var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Component } from 'preact';
var MessageType = (function (_super) {
    __extends(MessageType, _super);
    function MessageType() {
        var _this = _super.call(this) || this;
        _this.onVisibilityChange = function () { };
        _this.state = {
            visible: false,
            visibilityChanged: false,
            attachmentsVisible: true
        };
        return _this;
    }
    /**
     * Check if we have a timeout
     */
    MessageType.prototype.componentDidMount = function () {
        var _this = this;
        setTimeout(function () {
            _this.state.visible = true;
            _this.state.visibilityChanged = true;
            _this.onVisibilityChange();
            _this.props.onVisibilityChange(_this.props.message, _this.state);
        }, this.props.timeout || 0);
    };
    return MessageType;
}(Component));
export default MessageType;
//# sourceMappingURL=messagetype.js.map