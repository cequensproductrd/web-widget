var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { h } from 'preact';
import { botman } from '../botman';
import MessageType from "./messagetype";
var ListType = (function (_super) {
    __extends(ListType, _super);
    function ListType() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ListType.prototype.getButton = function (button) {
        var _this = this;
        if (button.type === 'postback') {
            return h("div", { class: "btn", onClick: function () { return _this.performAction(button); } }, button.title);
        }
        if (button.type === 'web_url') {
            return h("a", { class: "btn", href: button.url, target: "_blank" }, button.title);
        }
    };
    ListType.prototype.render = function (props) {
        var _this = this;
        var message = props.message;
        var globalButtons = message.globalButtons.map(function (button) {
            return _this.getButton(button);
        });
        var lists = message.elements.map(function (element) {
            var elementButtons = element.buttons.map(function (button) {
                return _this.getButton(button);
            });
            return h("div", { style: { minWidth: '200px' } },
                h("img", { src: element.image_url }),
                h("p", null, element.title),
                h("p", null, element.subtitle),
                elementButtons);
        });
        return (h("div", null,
            h("div", { style: { overflowX: 'scroll', display: 'flex' } }, lists),
            globalButtons));
    };
    ListType.prototype.performAction = function (button) {
        var _this = this;
        botman.callAPI(button.payload, true, null, function (msg) {
            _this.props.messageHandler({
                text: msg.text,
                type: msg.type,
                actions: msg.actions,
                attachment: msg.attachment,
                additionalParameters: msg.additionalParameters,
                from: 'chatbot'
            });
        }, null);
    };
    return ListType;
}(MessageType));
export default ListType;
//# sourceMappingURL=list.js.map