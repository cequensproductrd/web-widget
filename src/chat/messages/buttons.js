var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { h } from 'preact';
import { botman } from './../botman';
import MessageType from "./messagetype";
var ButtonsType = (function (_super) {
    __extends(ButtonsType, _super);
    function ButtonsType() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ButtonsType.prototype.render = function (props) {
        var _this = this;
        var message = props.message;
        var buttons = message.buttons.map(function (button) {
            if (button.type === 'postback') {
                return h("div", { class: "btn", onClick: function () { return _this.performAction(button); } }, button.title);
            }
            if (button.type === 'web_url') {
                return h("a", { class: "btn", href: button.url, target: "_blank" }, button.title);
            }
        });
        return (h("div", null,
            message.text,
            this.state.attachmentsVisible ? buttons : ''));
    };
    ButtonsType.prototype.performAction = function (button) {
        var _this = this;
        botman.callAPI(button.payload, true, null, function (msg) {
            _this.state.attachmentsVisible = false;
            _this.props.messageHandler({
                text: msg.text,
                type: msg.type,
                actions: msg.actions,
                attachment: msg.attachment,
                additionalParameters: msg.additionalParameters,
                from: 'chatbot'
            });
        }, null);
    };
    return ButtonsType;
}(MessageType));
export default ButtonsType;
//# sourceMappingURL=buttons.js.map