var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { h } from 'preact';
import MessageType from "./messagetype";
var TypingIndicator = (function (_super) {
    __extends(TypingIndicator, _super);
    function TypingIndicator() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.onVisibilityChange = function () {
            setTimeout(function () {
                _this.state.visible = false;
                _this.props.onVisibilityChange(_this.props.message, _this.state);
            }, _this.props.message.timeout * 1000);
        };
        return _this;
    }
    TypingIndicator.prototype.render = function (props) {
        return (h("div", { class: "loading-dots" },
            h("span", { class: "dot" }),
            h("span", { class: "dot" }),
            h("span", { class: "dot" })));
    };
    return TypingIndicator;
}(MessageType));
export default TypingIndicator;
//# sourceMappingURL=typing-indicator.js.map