var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { h } from "preact";
import MessageType from "./messagetype";
var TextType = (function (_super) {
    __extends(TextType, _super);
    function TextType() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TextType.prototype.render = function (props) {
        var message = props.message;
        var attachment = message.attachment;
        var textObject = { __html: message.text };
        return (h("div", null,
            h("p", { dangerouslySetInnerHTML: textObject }),
            attachment && attachment.type === "image" ? (h("img", { src: attachment.url, style: "max-width: 100%;" })) : (""),
            attachment && attachment.type === "audio" ? (h("audio", { controls: true, autoPlay: false, style: "max-width: 100%;" },
                h("source", { src: attachment.url, type: "audio/mp3" }))) : (""),
            attachment && attachment.type === "video" ? (h("video", { height: props.conf.videoHeight, controls: true, autoPlay: false, style: "max-width: 100%;" },
                h("source", { src: attachment.url, type: "video/mp4" }))) : ("")));
    };
    return TextType;
}(MessageType));
export default TextType;
//# sourceMappingURL=text.js.map