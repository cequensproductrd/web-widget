var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import { h, Component } from 'preact';
import { mobileClosedMessageAvatarStyle, closedChatAvatarImageStyle } from './style';
var ChatFloatingButton = (function (_super) {
    __extends(ChatFloatingButton, _super);
    function ChatFloatingButton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ChatFloatingButton.prototype.render = function (_a, _b) {
        var onClick = _a.onClick, conf = _a.conf;
        return (h("div", { style: { position: 'relative', cursor: 'pointer' }, onClick: this.props.onClick },
            h("div", { className: "mobile-closed-message-avatar", style: __assign({ background: conf.bubbleBackground }, mobileClosedMessageAvatarStyle) }, (conf.bubbleAvatarUrl === '') ?
                h("svg", { style: { paddingTop: 4 }, fill: "#FFFFFF", height: "24", viewBox: "0 0 24 24", width: "24", xmlns: "http://www.w3.org/2000/svg" },
                    h("path", { d: "M20 2H4c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zM6 9h12v2H6V9zm8 5H6v-2h8v2zm4-6H6V6h12v2z" }),
                    h("path", { d: "M0 0h24v24H0z", fill: "none" }))
                :
                    ((conf.bubbleAvatarUrl.indexOf('/') !== -1) ?
                        h("img", { src: conf.bubbleAvatarUrl, style: __assign({}, closedChatAvatarImageStyle) }) : h("div", { style: { display: 'flex', alignItems: 'center' } },
                        h("br", null),
                        conf.bubbleAvatarUrl)))));
    };
    return ChatFloatingButton;
}(Component));
export default ChatFloatingButton;
//# sourceMappingURL=chat-floating-button.js.map