var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import axios from 'axios';
import { h, Component } from 'preact';
import ChatFrame from './chat-frame';
import ChatFloatingButton from './chat-floating-button';
import ChatTitleMsg from './chat-title-msg';
import ArrowIcon from './arrow-icon';
import Api from './api';
import { desktopTitleStyle, desktopWrapperStyle, mobileOpenWrapperStyle, mobileClosedWrapperStyle, desktopClosedWrapperStyleChat } from './style';
import Echo from "laravel-echo";
var Widget = (function (_super) {
    __extends(Widget, _super);
    function Widget() {
        var _this = _super.call(this) || this;
        _this.toggle = function () {
            var stateData = {
                pristine: false,
                isChatOpen: !_this.state.isChatOpen,
                wasChatOpened: _this.state.wasChatOpened
            };
            if (!_this.state.isChatOpen && !_this.state.wasChatOpened) {
                if (_this.props.conf.sendWidgetOpenedEvent) {
                    setTimeout(function () {
                        _this.sendOpenEvent();
                    }, 500);
                }
                stateData.wasChatOpened = true;
            }
            _this.setState(stateData);
        };
        _this.state.isChatOpen = false;
        _this.state.pristine = true;
        _this.state.wasChatOpened = false;
        return _this;
    }
    Widget.prototype.componentDidMount = function () {
        window.botmanChatWidget = new Api(this);
        this.setupEcho();
        if (typeof this.props.conf.init === 'function') {
            this.props.conf.init(window.botmanChatWidget);
        }
    };
    Widget.prototype.setupEcho = function () {
        if (this.props.conf.useEcho === true) {
            this.Echo = new Echo(this.props.conf.echoConfiguration);
            // Join channel
            var channel = void 0;
            if (this.props.conf.echoChannelType === 'private') {
                channel = this.Echo.private(this.props.conf.echoChannel);
            }
            else {
                channel = this.Echo.channel(this.props.conf.echoChannel);
            }
            channel.listen(this.props.conf.echoEventName, function (message) {
                window.botmanChatWidget.writeToMessages(message);
            });
        }
    };
    Widget.prototype.render = function (props, state) {
        var conf = props.conf, isMobile = props.isMobile;
        var isChatOpen = state.isChatOpen, pristine = state.pristine;
        var wrapperWidth = { width: isMobile ? conf.mobileWidth : conf.desktopWidth };
        var desktopHeight = (window.innerHeight - 100 < conf.desktopHeight) ? window.innerHeight - 90 : conf.desktopHeight;
        conf.wrapperHeight = desktopHeight;
        var wrapperStyle;
        if (!isChatOpen && (isMobile || conf.alwaysUseFloatingButton)) {
            wrapperStyle = __assign({}, mobileClosedWrapperStyle); // closed mobile floating button
        }
        else if (!isMobile) {
            wrapperStyle = (isChatOpen || this.state.wasChatOpened) ?
                (isChatOpen) ? __assign({}, desktopWrapperStyle, wrapperWidth) : __assign({}, desktopClosedWrapperStyleChat)
                : __assign({}, desktopClosedWrapperStyleChat); // desktop mode, chat style
        }
        else {
            wrapperStyle = mobileOpenWrapperStyle; // open mobile wrapper should have no border
        }
        return (h("div", { style: wrapperStyle },
            (isMobile || conf.alwaysUseFloatingButton) && !isChatOpen ?
                h(ChatFloatingButton, { onClick: this.toggle, conf: conf })
                :
                    (isChatOpen || this.state.wasChatOpened) ?
                        (isChatOpen ?
                            h("div", { style: __assign({ background: conf.mainColor }, desktopTitleStyle), onClick: this.toggle },
                                h("div", { style: {
                                        display: 'flex', alignItems: 'center', padding: '0px 30px 0px 0px',
                                        fontSize: '15px', fontWeight: 'normal', color: conf.headerTextColor
                                    } }, conf.title),
                                h(ArrowIcon, { isOpened: isChatOpen })) : h(ChatTitleMsg, { onClick: this.toggle, conf: conf }))
                        :
                            h(ChatTitleMsg, { onClick: this.toggle, conf: conf }),
            h("div", { key: 'chatframe', style: {
                    display: isChatOpen ? 'block' : 'none',
                    height: isMobile ? conf.mobileHeight : desktopHeight
                } }, pristine ? null : h(ChatFrame, __assign({}, this.props)))));
    };
    Widget.prototype.open = function () {
        this.setState({
            pristine: false,
            isChatOpen: true,
            wasChatOpened: true
        });
    };
    Widget.prototype.close = function () {
        this.setState({
            pristine: false,
            isChatOpen: false
        });
    };
    Widget.prototype.sendOpenEvent = function () {
        var data = new FormData();
        data.append('driver', 'web');
        data.append('eventName', 'widgetOpened');
        data.append('eventData', this.props.conf.widgetOpenedEventData);
        axios.post(this.props.conf.chatServer, data).then(function (response) {
            var messages = response.data.messages || [];
            messages.forEach(function (message) {
                window.botmanChatWidget.writeToMessages(message);
            });
        });
    };
    return Widget;
}(Component));
export default Widget;
//# sourceMappingURL=widget.js.map