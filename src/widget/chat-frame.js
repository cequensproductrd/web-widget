var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import { h, Component } from 'preact';
var ChatFrame = (function (_super) {
    __extends(ChatFrame, _super);
    function ChatFrame() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ChatFrame.prototype.shouldComponentUpdate = function () {
        // do not re-render via diff:
        return false;
    };
    ChatFrame.prototype.render = function (_a, _b) {
        var iFrameSrc = _a.iFrameSrc, isMobile = _a.isMobile, conf = _a.conf;
        var dynamicConf = window.botmanWidget || {}; // these configuration are loaded when the chat frame is opened
        var encodedConf = encodeURIComponent(JSON.stringify(__assign({}, conf, dynamicConf)));
        return (h("iframe", { id: "chatBotManFrame", src: iFrameSrc + '?conf=' + encodedConf, width: '100%', height: isMobile ? '94%' : '100%', frameBorder: '0', allowTransparency: true, style: 'background-color:transparent' }));
    };
    return ChatFrame;
}(Component));
export default ChatFrame;
//# sourceMappingURL=chat-frame.js.map