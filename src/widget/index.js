var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import { h, render } from 'preact';
import Widget from './widget';
import { defaultConfiguration } from './configuration';
if (window.attachEvent) {
    window.attachEvent('onload', injectChat);
}
else {
    window.addEventListener('load', injectChat, false);
}
function getUrlParameter(name, defaults) {
    if (defaults === void 0) { defaults = ''; }
    name = name.replace(/[[]/, '\\[').replace(/[]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(document.getElementById('botmanWidget').getAttribute('src'));
    return results === null ? defaults : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
function getUserId(conf) {
    return conf.userId || generateRandomId();
}
function generateRandomId() {
    return Math.random().toString(36).substr(2, 6);
}
function injectChat() {
    var root = document.createElement('div');
    root.id = 'botmanWidgetRoot';
    document.getElementsByTagName('body')[0].appendChild(root);
    var settings = {};
    try {
        settings = JSON.parse(getUrlParameter('settings', '{}'));
    }
    catch (e) { }
    var dynamicConf = window.botmanWidget || {}; // these configuration are loaded when the chat frame is opened
    dynamicConf.userId = getUserId(__assign({}, defaultConfiguration, dynamicConf));
    if (typeof dynamicConf.echoChannel === 'function') {
        dynamicConf.echoChannel = dynamicConf.echoChannel(dynamicConf.userId);
    }
    var conf = __assign({}, defaultConfiguration, settings, dynamicConf);
    var iFrameSrc = conf.frameEndpoint;
    render(h(Widget, { isMobile: window.screen.width < 500, iFrameSrc: iFrameSrc, conf: conf }), root);
}
//# sourceMappingURL=index.js.map