var Api = (function () {
    function Api(widget) {
        this.widget = widget;
    }
    Api.prototype.open = function () {
        this.widget.open();
    };
    Api.prototype.close = function () {
        this.widget.close();
    };
    Api.prototype.toggle = function () {
        this.widget.toggle();
    };
    Api.prototype.isOpen = function () {
        return this.widget.state.isChatOpen === true;
    };
    Api.prototype.callChatWidget = function (payload) {
        if (this.isOpen()) {
            document.getElementById('chatBotManFrame').contentWindow.postMessage(payload, '*');
        }
        else {
            try {
                this.open();
                setTimeout(function () {
                    document.getElementById('chatBotManFrame').contentWindow.postMessage(payload, '*');
                }, 750);
            }
            catch (e) {
                console.error(e);
            }
        }
    };
    Api.prototype.writeToMessages = function (message) {
        this.callChatWidget({
            method: 'writeToMessages',
            params: [
                message
            ]
        });
    };
    Api.prototype.sayAsBot = function (text) {
        this.callChatWidget({
            method: 'sayAsBot',
            params: [
                text
            ]
        });
    };
    Api.prototype.say = function (text) {
        this.callChatWidget({
            method: 'say',
            params: [
                text
            ]
        });
    };
    Api.prototype.whisper = function (text) {
        this.callChatWidget({
            method: 'whisper',
            params: [
                text
            ]
        });
    };
    return Api;
}());
export default Api;
//# sourceMappingURL=api.js.map